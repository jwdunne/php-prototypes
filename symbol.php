<?php

namespace JWD\Symbol;

function symbols()
{
    $symbols = func_get_args();
    foreach ($symbols as $symbol) {
        symbol($symbol);
    }
}

function symbol($str)
{
    if (! defined($str)) {
        define($str, $str);
    }

    return $str;
}