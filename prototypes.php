<?php

namespace JWD\Prototypes;

class Object
{
    private $prototype;
    private $properties = array();

    public static function create($properties = array(), $prototype = null)
    {
        return new static($properties, $prototype);
    }
    
    public function __construct($properties = array(), $prototype = null)
    {
        foreach ($properties as $property => $value) {
            if ($value instanceof Closure) {
                $properties[$property] = $value->bindTo($this);
            }
        }
        
        $this->properties = $properties;
        $this->prototype = $prototype;
    }

    // Object magic.
    
    public function __get($property)
    {
        if (! isset($this->properties[$property])) {
            if ($this->prototype != null) {
                return $this->prototype->{$property};
            } else {
                // throw property not found
            }
        }
        
        return $this->properties[$property];
    }

    public function __set($property, $value)
    {
        return $this->properties[$property] = $value;
    }

    public function __call($property, $args)
    {
        if (! isset($this->properties[$property])) {
            if ($this->prototype != null) {
                array_unshift($args, $this);
                return call_user_func_array(
                    $this->prototype->{$property},
                    $args
                );
            } else {
                // throw method not found
            }
        }

        array_unshift($args, $this);
        return call_user_func_array(
            $this->properties[$property],
            $args
        );
    }

    public function __isset($property)
    {
        return isset($this->properties[$property]);
    }

    public function __unset($property)
    {
        unset($this->properties[$property]);
    }

    // Prototypes

    public function prototype($object = null)
    {
        if ($object == null) {
            return $this->prototype;
        }
        
        $this->prototype = $object;
    }
}
