<?php

use JWD\Prototypes\Object;

include 'prototypes.php';
include 'symbol.php';

JWD\Symbol\symbols(
    'sound',
    'makeSound',
    'hurt'
);

$animal = Object::create([
    sound => 'generic',
    makeSound => function ($self) {
        echo $self->sound . PHP_EOL;
    },
    hurt => function ($self) {
        echo 'owwwww!';
    }
]);

$animal->makeSound();

$dog = Object::create([
    makeSound => function ($self) {
        echo 'dog: ' . $self->sound . PHP_EOL;
    }
], $animal);

$dog->makeSound();

$germanSheperd = Object::create([
    sound => 'growwwwll'
], $dog);

$germanSheperd->makeSound();

$germanSheperd->prototype($animal);

$germanSheperd->makeSound();

$germanSheperd->attack = function ($animal) {
    $animal->hurt();
};

$germanSheperd->attack($dog);